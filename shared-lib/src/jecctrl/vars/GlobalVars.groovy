#!/usr/bin/env groovy
package jecctrl.vars

class GlobalVars{
    static def setenv(script, jobname)
    {   
        // Edit this with your subsystem variables
        switch(jobname) {
            case ~/.*-prod$/:
                script.env.image = "docker://gitlab-registry.cern.ch/tlampen/automation:prod"                
		        script.env.image_setup = "source /home/jecpcl/setup.sh; module load lxbatch/tzero" // Your image setup script
                script.env.dbinstance = "jec_test_v1" // The InfluxDB where you write - usually different between prod and repro
                script.env.campaign = "prod" // The campaign tag, can also be a list of campaigns with a space in between
//                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/kbdustfxdjyg3b88d96k3xwyoe" // The mattermost hook for notification - optional functionality
                script.env.eospath = "/eos/cms/store/group/example" // Where you want to save stuff etc...
                script.env.eosplots = "/eos/your-plots" // Where you want to save plots etc... - optional functionality
                script.env.plotsurl = "https://your-plots.web.cern.ch/" // URL were the plots are plublished - optional functionality
                break
            case ~/.*-repro$/:
                script.env.image = "docker://gitlab-registry.cern.ch/tlampen/automation:repro"
                script.env.image_setup = "source /home/jecpcl/setup.sh"
                script.env.dbinstance = "jec_test_v1"
                script.env.campaign = "all"
//                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/kbdustfxdjyg3b88d96k3xwyoe"
                script.env.eospath = "/eos/cms/store/group/example"
                script.env.eosplots = "/eos/your-plots"
                script.env.plotsurl = "https://your-plots.web.cern.ch/"
                break
            default:
                script.env.image = "docker://gitlab-registry.cern.ch/tlampen/automation:dev"
                script.env.image_setup = "source /home/jecpcl/setup.sh"
                script.env.dbinstance = "jec_test_v1"
                script.env.campaign = "dev"
//                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/y95qhxb5a7r49b5m818hjgqssh"
                script.env.eospath = "/eos/cms/store/group/example"
                script.env.eosplots = "/eos/your-plots"
                script.env.plotsurl = "https://your-plots-dev.web.cern.ch/"
                break
        }
    }
}
