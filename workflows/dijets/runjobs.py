#!/usr/bin/env python3
import sys, subprocess
from ecalautoctrl import HTCHandler, process_by_run, dbs_data_source

@dbs_data_source
@process_by_run

if __name__ == '__main__':
    handler = HTCHandler(task='jec-l2res',
                         dsetname='/JetHT*/*/*NANO*')

    subprocess.run(["ls", "-l"])
#    subprocess.run(["root -l -b -q mk_DijetHistosFill.C\(\"UL2016GH\"\)", "-l"])

    ret = handler(wflow='my-workflow')

    sys.exit(ret)

