import sys
from ecalautoctrl import HTCHandlerByRunDBS
        
if __name__ == '__main__':
    handler = HTCHandlerByRunDBS(task='dijet-analysis-worker',
                                 dsetname='/JetHT*/*/*NANO*')

    ret = handler()
    
    sys.exit(ret)
